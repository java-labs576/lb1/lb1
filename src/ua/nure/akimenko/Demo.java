package ua.nure.akimenko;

public class Demo {

	public static void main(String[] args) {
		var stor = new Storage();
		stor.add(new Car("Ford", "00121", "black"))
			.add(new Car("BMW", "97256", "red"))
			.add(new Car("Hyundai", "04576", "blue"))
			.add(new Car("Ford", "55306", "yellow"))
			.add(new Car("Ferrari", "06548", "green"))
			.add(new Car("Kia", "19567", "pink"))
			.add(new Car("Lamborgini", "00002", "white"))
			.add(new Car("Ford", "15447", "black"));

		System.out.println("Количество машин в хранилище: " + stor.getCarsNum());
		System.out.println("Список машин в хранилище:");
		System.out.println(stor);
		
		Car[] cars = stor.getCarsByModel("Ford");
		System.out.println("Количество машин модели \"Ford\": " + cars.length);
		System.out.println("Список машин модели \"Ford\":");
		for (Car car : cars) { 
			System.out.println(car);
		}
	}

}

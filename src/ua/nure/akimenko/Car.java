package ua.nure.akimenko;

public class Car {
	// марка
	private String model;
	
	private String number;
	
	private String color;

	public Car(String model, String number, String color) {
		this.model = model;
		this.number = number;
		this.color = color;
	}
	
	public boolean belongsToModel(String model) {
		return this.model == model;
	}

	@Override
	public String toString() {
		return "model: " + model + ", number: " + number
				+ ", color: " + color;
	}
}

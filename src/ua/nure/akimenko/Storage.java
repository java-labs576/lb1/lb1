package ua.nure.akimenko;

public class Storage {
	private Car[] arr = new Car[10];
	
	// last inserted index
	private int lastInsInd = -1;
	
	private void resize() {
		Car[] oldArr = arr;
		arr = new Car[2 * oldArr.length];
		System.arraycopy(oldArr, 0, arr, 0, oldArr.length);
	}
	
	public Storage add(Car car) {
		if (lastInsInd == arr.length - 1) { 
			resize();
		}
		arr[++lastInsInd] = car;
		return this;
	}
	
	public Car[] getCarsByModel(String model) {
		// число машин, которые принадлежан заданной модели
		int carNum = 0;
		
		for (int i = 0; i <= lastInsInd; ++i) {
			if (arr[i].belongsToModel(model)) {
				++carNum;
			}
		}
		
		Car[] cars = new Car[carNum];
		int j = 0;
		for (int i = 0; i <= lastInsInd; ++i) {
			if (arr[i].belongsToModel(model)) {
				cars[j] = arr[i];
				++j;
			}
		}		
		
		return cars;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= lastInsInd; ++i) {
			sb.append(arr[i])
				.append('\n');
		}
		return sb.toString();
	}
	
	// returns cars number
	public int getCarsNum() {
		return lastInsInd + 1;
	}
 }
